# Accessibility

This is a place to report, discuss and resolve the current accessibility issues with riseup.net services

* [Full list of current problems](https://0xacab.org/riseup/accessibility/issues)
* [Current status of issues](https://0xacab.org/riseup/accessibility/boards)
* [Report a new problem](https://0xacab.org/riseup/accessibility/issues/new)
